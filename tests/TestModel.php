<?php declare(strict_types=1);
use PHPUnit\Framework\TestCase;
require_once "./docker-web/www/Models/Model.php";

class TestModel extends TestCase
{
    /**
     * Test pour vérifier que getLast renvoie un array
     */
    public function testGetLastIsArray() {
        /*$this->assertInstanceOf(
            Array::class,
            Model::getLast()
        );*/
        $model = Model::getModel();
        $variable = $model->getLast();
        $this->assertIsArray($variable);
    }

    /**
     * Test pour vérifier que getLast renvoie une liste de taille 25
     */
    public function getLastReturnArrayOfSize25() {
        $this->assertEquals(
            25,
            count(Model::getLast())
        );
    }
}